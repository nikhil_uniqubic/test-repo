# Solomondigtial - Wallpaper

Compatible with Xcode 13 and Swift 5.3

# Steps to run the project
- Clone the project
- Go to terminal > set project path
- Run 
	- `Pod install`

# Steps to archive the project
- Make sure the build number abd build version is same for all the targets otherwise build upload might fail
- Select any device from device drop down list
- Select Product -> Archive
	
### Targets
==========================
- Wallpaper
    Main target of the application
- KeyboardExtension
    For custom keyboard with background image
- WidgetExtention
   For custom widgets of Clock, Calender and Static Image
-  WidgetIntentHandler
    For widget configutaion. Responsible for fetching configurayions from API and passing it on to widget
    
